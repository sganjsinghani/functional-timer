import React from "react";
import "./App.css";
import Timer from "./Timer";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      timerMounted: false,
      timerStarted: false,
    };
    this.addTimer = this.addTimer.bind(this);
    this.changeTimerStatus = this.changeTimerStatus.bind(this);
  }

  componentDidMount() {
    console.log("Application mounted");
  }

  changeTimerStatus() {
    this.setState({ timerStarted: !this.state.timerStarted }, () => {
      if (this.state.timerStarted) alert("Timer started");
    });
  }

  addTimer() {
    const newState = {
      timerMounted: !this.state.timerMounted,
      timerStarted: this.state.timerMounted ? false : this.state.timerStarted,
    };

    this.setState({ ...newState });
  }

  render() {
    return (
      <div className="App">
        <h1>Timer</h1>
        <button onClick={this.addTimer}>
          {this.state.timerMounted ? "REMOVE TIMER" : "ADD Timer"}
        </button>
        <br />
        <br />
        {this.state.timerMounted && (
          <>
            <button onClick={this.changeTimerStatus}>
              {this.state.timerStarted ? "STOP" : "START"}
            </button>
            <br />
            <br />
            <Timer timerStarted={this.state.timerStarted} />
          </>
        )}
      </div>
    );
  }
}

export default App;
